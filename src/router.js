import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import LandingPage from './views/LandingPage.vue';
import Content from './views/Content.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/:lang/hjem',
      name: 'home',
      props: true,
      component: Home
    },
    {
      path: '/:lang/kapittler',
      name: 'landing-page',
      props: true,
      component: LandingPage
    },
    {
      path: '/:lang/kapittel/:chapter',
      name: 'content',
      props: true,
      component: Content
    },
  ]
});
